<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function atrium_invoicing_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function atrium_invoicing_node_info() {
  $items = array(
    'invoice' => array(
      'name' => t('Invoice'),
      'module' => 'features',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '0',
      'body_label' => '',
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function atrium_invoicing_views_api() {
  return array(
    'api' => '2',
  );
}
